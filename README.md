# arch-bootstrap

Bootstrap script for my Arch Linux configuration.

Just clone this repo and copy the script into the root directory.

`git clone https://gitlab.com/danieltpye/arch-bootstrap.git`

and run the arch-bootstrap script

`bash arch-bootstrap.sh`

After that an ncurses command line prompt will guide you through the installation process.

## Prerequisites

It's expected that the script be run from the root directory of a fresh install of Arch Linux after the initial reboot.

For detailed instructions on the initial installation steps for Arch see the [ArchWiki](https://wiki.archlinux.org/index.php/Installation_guide).

## Basic Arch install

### Set keyboard layout
* loadkeys se-lat6

### Connect to the internet
* wifi-menu
* ping -c 5 archlinux.org

### Update system clock
* timedatectl set-ntp true

### Partition disks
* gdisk -l
* gdisk /dev/sdX

### Format partitions and edit bootloader file
*Partition with one EFI partition and one root partition in ext4 format*
* mkfs.ext4 /dev/sdaX
* mkfs.fat -F32 /dev/sdaX

### Mount filesystems
* mount /dev/sda2 /mnt
* mkdir /mnt/boot
* mount /dev/sda1 /mnt/boot

### Select mirrors
* vi /etc/pacman.d/mirrorlist

### Install base packages
* pacstrap /mnt base

### Configure system
* genfstab -U /mnt >> /mnt/etc/fstab
* vi /mnt/etc/fstab
* arch-chroot /mnt
* ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
* hwclock --systohc
* vi /etc/locale.gen # Uncomment locale(s)
* locale-gen
* vi /etc/locale.conf # Add LANG=en_GB.UTF-8
* vi /etc/vconsole.conf # Add KEYMAP=se-lat6
* vi /etc/hostname # Add hostname
* vi /etc/hosts # Add matching entries
```
127.0.0.1	localhost
::1		localhost
127.0.1.1	myhostname.localdomain	myhostname
```

### Initramfs
* mkinitcpio -p linux

### Set root password
* passwd

### Install microcode and network packages
* pacman -S intel-ucode iw dialog wpa_supplicant

#### Install bootloader
* bootctl --path=/boot install
* lsblk -f

* vi /boot/loader/entries/arch.conf
```
title Arch Linux
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
options root=/dev/sdaX
```

* exit
* reboot

### Post-installation
* systemctl enable dhcpcd.service # If using a VM, otherwise configure NetworkManager as normal

